---
layout: default
title: Issue 机器人
parent: 实践模式
nav_order: 7
---


Intellij 相关的 IDEA 会在异常时：

1. 提供创建 Issue 的选项
2. 如果 issue 已经存在，会通过 comments 的方式创建。

示例：[[auto-generated:-1738221085] null](https://github.com/pest-parser/intellij-pest/issues/214)

```
    Plugin Name: Pest
    Plugin Version: 0.3.2
    Rust Plugin Version: 0.4.178.4873-222
    Bundled Pest Version: 0.1.3
    OS Name: Mac OS X
    Java Version: 17.0.4
    App Name: CLion
    App Full Name: CLion
    App Version name: CLion
    Is EAP: false
    App Build: CL-222.4167.25
    App Version: 2022.2.2
    Last Action:
    title: [auto-generated:-1738221085] null
```


